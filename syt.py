#!/usr/bin/env python3


def suurim_yhistegur(a, b):
    while b > 0:
        uus = a % b
        a = b
        b = uus
    return a

def main():
    n = suurim_yhistegur(15, 6)
    print(str(n))
    
 
if __name__ == '__main__':
    main()

